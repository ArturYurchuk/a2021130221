package org.example;
import java.util.Scanner;

public class Calculadora {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Digite o primeiro número: ");
        double numero1 = scanner.nextDouble();

        System.out.print("Digite a operação (+, -, *, /): ");
        char operacao = scanner.next().charAt(0);

        System.out.print("Digite o segundo número: ");
        double numero2 = scanner.nextDouble();


        switch (operacao) {
            case '+':
                System.out.println("Resultado: " + add(numero1,numero2));
                break;
            case '-':
                System.out.println("Resultado: " + sub(numero1,numero2));
                break;
            case '*':
                System.out.println("Resultado: " + mult(numero1,numero2));
                break;
            case '/':
                if (numero2 != 0) {
                    System.out.println("Resultado: " + div(numero1,numero2));
                } else {
                    System.out.println("Não é possível dividir por zero.");
                }
                break;
            default:
                System.out.println("Operação inválida.");
        }
    }

    public static double add(double n1, double n2){
        return n1+n2;
    }
    public static double sub(double n1, double n2){
        return n1-n2;
    }
    public static double mult(double n1, double n2){
        return n1*n2;
    }
    public static double div(double n1, double n2){
        return (double) n1 /n2;
    }
}
