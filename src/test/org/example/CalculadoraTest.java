package org.example;

import static org.junit.Assert.*;

public class CalculadoraTest {

    @org.junit.Test
    public void add() {
        assertEquals(5.0,Calculadora.add(3,2),0.01);
    }

    @org.junit.Test
    public void sub() {
        assertEquals(1.0,Calculadora.sub(3,2),0.01);
    }

    @org.junit.Test
    public void mult() {
        assertEquals(6.0,Calculadora.mult(3,2),0.01);
    }

    @org.junit.Test
    public void div() {
        assertEquals(2.0,Calculadora.sub(4,2),0.01);
    }
}